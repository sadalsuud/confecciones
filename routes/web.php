<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {

    Route::resource('empleado', 'EmpleadoController');
    Route::resource('proveedor', 'ProveedorController');
    Route::resource('inventario', 'InventarioController');
    Route::resource('cliente', 'ClienteController');
    Route::resource('contabilidad', 'ContabilidadController');
    Route::resource('producto', 'ProductoController');
    Route::resource('pedido', 'PedidoController');

    Route::post('getProveedores', 'ProveedorController@getProveedores');
    Route::post('getProducto', 'ProductoController@getProducto');
    Route::get('historico-empleados', 'EmpleadoController@historico');
    Route::post('eliminar-empleado', 'EmpleadoController@eliminar');
    Route::post('restaurar-empleado', 'EmpleadoController@restaurar')->name('restaurar-empleado');
    Route::post('agregarEjemplar', 'ProductoController@agregarEjemplar');
    Route::post('cobrarPedido', 'PedidoController@cobrarPedido');
    Route::any('liquidar-nomina', 'ContabilidadController@liquidarNomina');
    Route::any('registrar-liquidacion-nomina', 'ContabilidadController@guardarLiquidarNomina');
    Route::get('descargar-pedidio/{id}', 'PedidoController@pdf')->name('pedido.pdf');
    Route::get('validadCantidad', 'ProductoController@validarCantidad');
});
