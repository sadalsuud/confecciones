@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Informacion de clientes
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span>Registrar
                                              </button>
                                            </div>
                                        </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbClientes" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombres</th>
                                    <th>Documento</th>
                                    <th>Teléfono</th>
                                    <th>Dirección</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clientes as $cliente)
                                    <tr>
                                        <td>{{ $cliente->id}}</td>
                                        <td>{{ $cliente->nombre }}</td>
                                        <td>{{ $cliente->documento }}</td>
                                        <td>{{ $cliente->telefono }}</td>
                                        <td>{{ $cliente->direccion }}</td>
                                        <td><a class='btn btn-danger eliminar'><span class='fas fa-trash-alt'></span></a>
                                            <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo">
                                                <span class='fas fa-edit'></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombres</th>
                                    <th>Documento</th>
                                    <th>Teléfono</th>
                                    <th>Dirección</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal -->
            <form id="frmajax" method="POST" action="{{ url('cliente') }}">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Registro de cliente</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="nombre">Nombres:</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombres" name="nombre">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="" class="col-sm-10">Número Documento:</label>
                                        <input type="number" placeholder="Numero Documento" class="form-control" name="documento">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="Telf" class="col-sm-10">Telefono:</label>
                                        <input type="number" placeholder="# Telefono" class="form-control" name="telefono">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="direccion" class="col-sm-10">Dirección:</label>
                                        <input type="text" id="direccion" placeholder="Dirección" class="form-control" name="direccion">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" id="btnguardar" type="submit">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>


    <script>
        $(document).ready(function () {

            $('#tbClientes').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#tbClientes tbody').on('click', '.editar', function () {

                var fila = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/cliente/" + fila,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        $("input[name='id']").val(rta.id);
                        $("input[name='nombre']").val(rta.nombre);
                        $("input[name='documento']").val(rta.documento);
                        $("input[name='telefono']").val(rta.telefono);
                        $("input[name='direccion']").val(rta.direccion);

                        $("#frmajax").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#frmajax").attr('action', '{{ url('/cliente') }}/' + rta.id);
                    }
                });
            });

            $('#tbClientes tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var nombre = $(this).parent().parent().children("td").eq(1).html();
                var documento = $(this).parent().parent().children("td").eq(2).html();

                var aux = documento + ": " + nombre;

                alertify.confirm("¿Desea eliminar a \n" + aux + "?",

                    function () {
                        $.ajax({
                            url: "/cliente/" + id,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rta) {
                                if (rta.OK == 1) {
                                    fila.remove();
                                    alertify.success('Borrado ' + aux);
                                }
                                else{
                                    alertify.success(rta.Error);
                                }
                            }
                        });
                    }
                );
            });
        });
    </script>
@endsection

