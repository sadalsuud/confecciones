@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Libro de contabilidad
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    {{--<span class="pull-right">--}}
                                    {{--<div class="btn-group">--}}
                                    {{--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">--}}
                                    {{--<span class="fa fa-users"></span> Liquidar--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    {{--</span>--}}
                                </h4>
                            </div>
                        </div>

                        <div class="box-body">

                            <form action="{{ url('liquidar-nomina') }}" id="formLiquidar" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-10">

                                        <div class="form-group col-xs-12 col-sm-4">
                                            <label for="concepto">Desde:</label>
                                            <input type="date" class="form-control" id="desde" name="desde">
                                        </div>

                                        <div class="form-group col-xs-12 col-sm-4">
                                            <label for="cantidad">Hasta:</label>
                                            <input type="date" class="form-control" id="hasta" name="hasta">
                                        </div>

                                        <div class="form-group col-xs-12 col-sm-4">
                                            <label for="cantidad"></label>
                                            <div>
                                                <button class="btn btn-info pull-left" type="submit">
                                                    <i class="fas fa-save"></i> Liquidar
                                                </button>
                                                <button class="btn btn-success pull-right" id="btnGuardarLiquidacion">
                                                    <i class="fas fa-save"></i> Guardar liquidación
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                            </form>
                        </div>
                        <div class="box-body">

                            <table id="tbNomina" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Documento</th>
                                    <th>Tipo contratacion</th>
                                    <th>Producción</th>
                                    <th>Liquidación</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($empleados)
                                    @foreach($empleados as $empleado)
                                        <tr>
                                            <td>{{ $empleado['nombre']}}</td>
                                            <td>{{ $empleado['documento'] }}</td>
                                            <td>{{ $empleado['tipo_contratacion'] }}</td>
                                            <td>{{ $empleado['produccion'] }}</td>
                                            <td class="sueldos">{{ $empleado['liquidacion'] }}</td>
                                        </tr>
                                    @endforeach
                                @endisset
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Documento</th>
                                    <th>Tipo contratacion</th>
                                    <th>Producción</th>
                                    <th>Liquidación</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbNomina').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $("#btnGuardarLiquidacion").click(function (event) {
                event.preventDefault();

                var totalSueldos = 0;
                $(".sueldos").each(function (item) {
                    totalSueldos += parseInt($(this).text());
                });

                if (totalSueldos != 0) {
                    alertify.confirm("¿Desea registrar esta liquidación en el libro de cuentas, son en total $" + totalSueldos + "?",

                        function () {
                            $.ajax({
                                url: "/registrar-liquidacion-nomina",
                                type: 'post',
                                dataType: 'json',
                                data: {"nomina": totalSueldos},
                                success: function (rta) {

                                    if (rta.OK == 1) {
                                        alertify.success('Nomina registrada con éxito en el libro de contabilidad');
                                    }
                                }
                            });
                        }
                    );
                }
                else{
                    alertify.error("No ha generado aún la liquidación para fechas válidas");
                }
            });
        });

    </script>
@endsection

