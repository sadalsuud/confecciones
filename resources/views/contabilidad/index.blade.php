@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>

@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Libro de contabilidad
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span> Anotar
                                              </button>
                                            </div>
                                    </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbContabilidad" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Tipo</th>
                                    <th>Concepto</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Valor unitario</th>
                                    <th>Total</th>
                                    <th>Saldo</th>
                                    <th>Origen</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cuentas as $registro)
                                    <tr>
                                        <td>{{ $registro->id}}</td>
                                        @if($registro->tipo)
                                            <td>Entrada</td>
                                        @else
                                            <td>Salida</td>
                                        @endif
                                        <td>{{ $registro->concepto }}</td>
                                        <td>{{ $registro->cantidad }}</td>
                                        <td>{{ $registro->unidad }}</td>
                                        <td>{{ $registro->valor_unidad }}</td>
                                        <td>{{ $registro->total }}</td>
                                        <td>{{ $registro->saldo }}</td>
                                        @if(!is_null($registro->proveedor_id))
                                            <td>{{ $registro->proveedor->nombre }}</td>
                                        @elseif(!is_null($registro->pedido_id))
                                            <td>{{ $registro->pedido->cliente->nombre }}</td>
                                        @elseif(is_null($registro->proveedor_id) && is_null($registro->pedido_id))
                                            <td>---</td>
                                        @endif
                                        <td><a class='btn btn-danger eliminar'><span class='fas fa-trash-alt'></span></a>
                                            <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo">
                                                <span class='fas fa-edit'></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Tipo</th>
                                    <th>Concepto</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Valor unitario</th>
                                    <th>Total</th>
                                    <th>Saldo</th>
                                    <th>Origen</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <form action="{{ url('/contabilidad') }}" method="POST" id="frmajax">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Anotación en el libro de contabilidad</h4>
                            </div>
                            <div class="modal-body">

                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-12">
                                        <label>Tipo:</label>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="radio1" name="tipo" value="1">
                                            <label class="radio-inline" for="radio1">Entrada</label>
                                            <input type="radio" class="form-check-input" id="radio2" name="tipo" value="0" checked>
                                            <label class="radio-inline" for="radio2">Salida</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="concepto">Concepto:</label>
                                        <input type="text" class="form-control" id="concepto" placeholder="Concepto" name="concepto">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="cantidad">Cantidad:</label>
                                        <input type="number" min="0" class="form-control" id="cantidad" placeholder="Cantidad" name="cantidad">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="cantidad">Unidad:</label>
                                        <input type="text" class="form-control" id="unidad" placeholder="Unidades" name="unidad">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="valor_unitario">Valor unitario:</label>
                                        <input type="number" min="0" class="form-control" id="valor_unitario" placeholder="Valor unitario"
                                               name="valor_unidad">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="total">Total:</label>
                                        <input type="number" min="0" class="form-control" id="total" placeholder="Total" name="total" readonly>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="tipDoc">Proveedor</label>
                                        <select name="proveedor_id" class="form-control" id="proveedor_id">
                                            <option value="">Seleccione...</option>
                                            @foreach($proveedores as $index => $proveedor)
                                                <option value="{{ $index }}">{{ $proveedor }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" type="submit">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbContabilidad').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#valor_unitario').blur(function () {
                var total = parseInt($("#cantidad").val()) * parseInt($("#valor_unitario").val());
                $('#total').val(total);
            });

            $("#radio2:checked").change(function () {
                $.ajax({
                    url: "/getProveedores",
                    type: 'POST',
                    dataType: 'json',
                    success: function (rta) {
                        var selectProveedores = $("#proveedor_id");
                        selectProveedores.empty();
                        selectProveedores.append("<option value=''>Seleccione...</option>");
                        $.each(rta, function (index, value) {
                            selectProveedores.append("<option value='" + index + "'>" + value + "</option>");
                        });
                    }
                });
            });

            $("#radio1:checked").change(function () {
                alert("sdadsad");
                $("#proveedor_id").prop('readonly', true);
            });

            $('#tbContabilidad tbody').on('click', '.editar', function () {

                var id = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/contabilidad/" + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        $("input[name='id']").val(rta.id);
                        $("input[name='tipo'][value='" + rta.tipo + "']").prop('checked', true);
                        $("input[name='concepto']").val(rta.concepto);
                        $("input[name='cantidad']").val(rta.cantidad);
                        $("input[name='unidad']").val(rta.unidad);
                        $("input[name='valor_unitario']").val(rta.valor_unitario);
                        $("input[name='total']").val(rta.total);
                        $("select[name='proveedo_id']").val(rta.proveedor_id);

                        $("#frmajax").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#frmajax").attr('action', '{{ url('/contabilidad') }}/' + rta.id);
                    }
                });
            });

            $('#tbContabilidad tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var concepto = $(this).parent().parent().children("td").eq(2).html();
                var total = $(this).parent().parent().children("td").eq(6).html();

                var aux = concepto + ": $" + total;

                alertify.confirm("¿Desea eliminar el registro de " + aux + "?",

                    function () {
                        $.ajax({
                            url: "/contabilidad/" + id,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rta) {
                                if (rta.OK == 1) {
                                    fila.remove();
                                    alertify.success('Borrado ' + aux);
                                }
                            }
                        });
                    }
                );
            });

            $("#mimodalejemplo").on('show.bs.modal', function () {
                limpiarFormulario();
            });

            function limpiarFormulario() {
                var formulario = $("#frmajax");
                formulario.find("input[name='_method']").remove();
                formulario.attr('action', '{{ url('/contabilidad') }}');
                formulario[0].reset();
            }

        });

    </script>
@endsection

