@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Informacion del inventario
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span>Registrar
                                              </button>
                                            </div>
                                        </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbInventario" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Serial</th>
                                    <th>Cantidad</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($herramientas as $herramienta)
                                    @if($herramienta->estado)
                                        <tr>
                                    @else
                                        <tr class="danger">
                                            @endif
                                            <td>{{ $herramienta->id}}</td>
                                            <td>{{ $herramienta->nombre }}</td>
                                            <td>{{ $herramienta->serial }}</td>
                                            <td>{{ $herramienta->cantidad }}</td>
                                            <td>{{ $herramienta->fecha_ingreso }}</td>
                                            <td><a class='btn btn-danger' href='modal' data-mm='modal'><span class='fas fa-trash-alt'></span></a>
                                                <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo"><span
                                                            class='fas fa-edit'></span></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Serial</th>
                                    <th>Cantidad</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal -->
            <form id="frmajax" method="POST" action="{{ url('inventario') }}">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Registro de proveedor</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="nombre">Nombre:</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre">
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="apellido">Serial:</label>
                                        <input type="text" class="form-control" id="nit" placeholder="Código de serie" name="serial">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="Telf" class="col-sm-10">Cantidad:</label>
                                        <input type="number" min="0" placeholder="Cantidad" class="form-control" name="cantidad" value="1">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="date" class="col-2 col-form-label">Fecha Ingreso</label>
                                        <div class="col-10">
                                            <input name="fecha_ingreso" class="form-control" type="date" value="" id="example-date-input">
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <h5><label>Estado:</label></h5>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="radio1" name="estado" value="1">
                                            <label class="radio-inline" for="radio1">Funciona</label>
                                            <input type="radio" class="form-check-input" id="radio2" name="estado" value="0">
                                            <label class="radio-inline" for="radio2">Dañada</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" id="btnguardar" type="submit">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbInventario').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#tbInventario tbody').on('click', '.editar', function () {

                var fila = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/inventario/" + fila,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        $("input[name='id']").val(rta.id);
                        $("input[name='nombre']").val(rta.nombre);
                        $("input[name='serial']").val(rta.serial);
                        $("input[name='cantidad']").val(rta.cantidad);
                        $("input[name='fecha_ingreso']").val(rta.fecha_ingreso);
                        $("input[name='estado'][value='" + rta.estado + "']").prop('checked', true);

                        $("#frmajax").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#frmajax").attr('action', '{{ url('/inventario') }}/' + rta.id);
                    }
                });
            });
        });
    </script>
@endsection

