<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <div class="user-panel">
            <div class="pull-left image">
                <img class="img-circle" src="../imagen/mujer.png" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
            </div>
        </div>

        <ul class="sidebar-menu tree" data-widget="tree">
            <div class="user-panel">
                <div class="pull-left">
                    <img style="width: 100%" class="img-circle" href="index.php" src="../imagen/logo.png" alt="User Image">
                </div>
            </div>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Recursos Humanos</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ url('/empleado') }}">
                            <i class="far fa-check-circle"></i>
                            Empleado
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ url('/historico-empleados') }}">
                            <i class="far fa-check-circle"></i>
                            Historial
                        </a>
                    </li>
                </ul>

            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fas fa-calculator"> </i>
                    <span>Finanzas</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="">
                        <a href="{{ url('/contabilidad') }}">
                            <i class="far fa-check-circle"></i>
                            Libro de contabilidad
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ url('/liquidar-nomina') }}">
                            <i class="far fa-check-circle"></i>
                            Liquidar nomina
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fas fa-cogs"></i>
                    <span>Produccion</span>
                    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ url('/producto') }}">
                            <i class="far fa-check-circle"></i>
                            Productos
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Clientes & Proveedores</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ url('/cliente') }}">
                            <i class="far fa-check-circle"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ url('/proveedor') }}">
                            <i class="far fa-check-circle"></i>
                            Proveedores
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Inventario</span>
                    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ url('/inventario') }}">
                            <i class="far fa-check-circle"></i>
                            Elementos
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-box"></i>
                    <span>Pedido</span>
                    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ url('/pedido') }}">
                            <i class="far fa-check-circle"></i>
                            Registrar Pedido
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

    </section>
</aside>
