<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/skins/skin-purple-light.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/hover-min.css') }}">

    @yield('styles')

</head>
<body class="skin-purple-light">

<div class="wrapper">
@Auth
    @include('layouts.cabecera')
    @include('layouts.menu')
@endauth

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- /.content-wrapper -->
</div>

<footer class="main-footer">ECRSOFT © 2018
    <span class="hidden-xs"> Version <b>1.0</b></span></footer>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });

    });
</script>

@yield('scripts')
</body>
</html>
