<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">

    <title>DISEÑO & MODA</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
</head>
<body>

<div class="container">
    <img src="../imagen/log.png">

    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf

        <div class="form-input">
            <i class="fa fa-user fa-2x cust" aria-hidden=true></i>
            <input id="email" type="email" placeholder="Usuario" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                   value="{{ old('email') }}" required autofocus>
        </div>

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <div class="form-input">
            <i class="fa fa-lock fa-2x cust" aria-hidden=true></i>
            <input id="password" placeholder="Contraseña" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                   required>
        </div>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ "Recuérdame" }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn-login"><i class="fas fa-sign-in-alt"></i>Login</button>
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ "Olvidó su clave" }}
                </a>
            </div>
        </div>
    </form>
</div>

</body>
</html>
