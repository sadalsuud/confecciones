@extends('layouts.app')


@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Informacion Financiera
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>

    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-check-circle-o">
                </i>
                <h2 class="box-title">Proceso Financiero</h2>
            </div>
            <div class="box-body no-padding clearfix">
                <p style="padding: 10px;">
                    Diseño & Moda ha dispuesto los siguientes modulos
                </p>
                <div class="table-responsive">
                    <table class="table table-hover" style="vertical-align: middle;">
                        <tbody>
                        <tr>
                            <th> Informacion RH</th>
                            <td>
                                <i class="fa fa-2x fa-check-square-o text-success hvr-grow"></i>
                            </td>
                        </tr>
                        <tr>
                            <th> Informacion Financiera</th>
                            <td>

                                <i class="fa fa-2x fa-check-square-o text-success hvr-grow"></i>
                            </td>
                        </tr>
                        <tr>
                            <th> Informacion Produccion</th>
                            <td>

                                <i class="fa fa-2x fa-check-square-o text-success hvr-grow"></i>
                            </td>
                        </tr>

                        <tr>
                            <th> Inventario</th>
                            <td>

                                <i class="fa fa-2x fa-check-square-o text-success hvr-grow"></i>
                            </td>
                        </tr>

                        <tr>
                            <th> Cotizacion</th>
                            <td>

                                <i class="fa fa-2x fa-check-square-o text-success hvr-grow"></i>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="row">
            <!--Nomina -->
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-check-circle">
                            </i>
                        </h3>
                        <p>Empleados</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a class="small-box-footer" href="empleado.php">Modulo Nomina <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!--Fin Nomina -->
            <!--Inventario -->
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-check-circle">
                            </i>
                        </h3>
                        <p>Inventario</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-box-open"></i>
                    </div>
                    <a class="small-box-footer" href="inventario.php">Modulo Inventario <i class="fa fa-arrow-circle-right"></i> </a>
                </div>
            </div>
            <!--Fin Inventario -->
            <!--Facturacion -->
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-check-circle">
                            </i>
                        </h3>
                        <p>Facturación</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <a class="small-box-footer" href="facturacion.html">Modulo Facturacion <i class="fa fa-arrow-circle-right"></i> </a>
                </div>
            </div>
            <!--Fin Facturación -->

            <!--Facturacion -->
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-check-circle">
                            </i>
                        </h3>
                        <p>Pedido</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-dolly"></i>
                    </div>
                    <a class="small-box-footer" href="pedido.html">Modulo Pedido <i class="fa fa-arrow-circle-right"></i> </a>
                </div>
            </div>
            <!--Fin Facturación -->
        </div>
    </section>

@endsection


