@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Historico Empleados
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>Consulta General</h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbEmpleados" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Documento</th>
                                    <th>Tipo Contratacion</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Fecha Eliminado</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($eliminados as $empleado)
                                    <tr class="danger">
                                        <td>{{ $empleado->id}}</td>
                                        <td>{{ $empleado->nombre }}</td>
                                        <td>{{ $empleado->apellidos }}</td>
                                        <td>{{ $empleado->documento }}</td>
                                        @if($empleado->tipo_contratacion == "TC")
                                            <td>Tiempo completo</td>
                                        @else
                                            <td>Ocasional</td>
                                        @endif
                                        <td>{{ $empleado->fecha_ingreso }}</td>
                                        <td>{{ $empleado->deleted_at }}</td>
                                        <td>
                                            <a class='btn btn-success restaurar'><span class='fas fa-undo-alt'></span></a>
                                            <a class='btn btn-danger eliminar'><span class='fas fa-trash-alt'></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Documento</th>
                                    <th>Tipo Contratacion</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Fecha Eliminado</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbEmpleados').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#tbEmpleados tbody').on('click', '.restaurar', function () {

                var id = $(this).parent().parent().children("td").html();
                var fila = $(this).parent().parent();

                $.ajax({
                    url: "/retaurar-empleado/",
                    type: 'post',
                    dataType: 'json',
                    data: {"id": id},
                    success: function (rta) {
                        fila.remove();
                        alertify.success(rta.ok);
                    }
                });
            });

            $('#tbEmpleados tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var nombre = $(this).parent().parent().children("td").eq(1).html();
                var apellidos = $(this).parent().parent().children("td").eq(2).html();
                var documento = $(this).parent().parent().children("td").eq(3).html();

                var aux = documento + ": " + nombre + " " + apellidos;

                alertify.confirm("¿Desea eliminar permanente a \n" + aux + "?",

                    function () {
                        $.ajax({
                            url: "/eliminar-empleado",
                            type: 'post',
                            dataType: 'json',
                            data: {"id": id},
                            success: function (rta) {
                                fila.remove();
                                alertify.success('Borrado ' + documento + ": " + nombre + " " + apellidos);
                            }
                        });
                    }
                );
            });
        });
    </script>
@endsection

