@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Informacion Empleados
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span>Registrar
                                              </button>
                                            </div>
                                        </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbEmpleados" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombres</th>
                                    <th>Documento</th>
                                    <th>Tipo Contratacion</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Sueldo</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($empleados as $empleado)
                                    <tr>
                                        <td>{{ $empleado->id}}</td>
                                        <td>{{ $empleado->nombre." ".$empleado->apellidos}}</td>
                                        <td>{{ $empleado->documento }}</td>
                                        @if($empleado->tipo_contratacion == "TC")
                                            <td>Tiempo completo</td>
                                        @else
                                            <td>Ocasional</td>
                                        @endif
                                        <td>{{ $empleado->fecha_ingreso }}</td>
                                        <td>{{ $empleado->sueldo }}</td>
                                        <td><a class='btn btn-danger eliminar'><span class='fas fa-trash-alt'></span></a>
                                            <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo">
                                                <span class='fas fa-edit'></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Nombres</th>
                                    <th>Documento</th>
                                    <th>Tipo Contratacion</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Sueldo</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal -->
            <form id="frmajax" method="POST" action="{{ url('empleado') }}">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Registro Empleado</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <input type="hidden" id="id" name="id">

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="nombre">Nombres:</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombres" name="nombre">
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="apellido">Apellidos:</label>
                                        <input type="text" class="form-control" id="apellido" placeholder="Apellidos" name="apellidos">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="example-number-input" class="col-2 col-form-label">Edad:</label>
                                        <input class="form-control" type="number" value="18" placeholder="Edad" name="edad">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="" class="col-sm-10">Documento:</label>
                                        <input type="number" placeholder="Numero Documento" class="form-control" name="documento">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="Telf" class="col-sm-10">Telefono:</label>
                                        <input type="number" placeholder="# Telefono" class="form-control" name="telefono">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="TipCont" class="col-sm-10">Tipo Contratacion:</label>
                                        <select name="tipo_contratacion" class="form-control">
                                            <option value="">Seleccione Tipo</option>
                                            <option value="TC">Tiempo Completo</option>
                                            <option value="O">Ocasional</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="date" class="col-2 col-form-label">Fecha Ingreso</label>
                                            <input name="fecha_ingreso" class="form-control" type="date" value="" id="example-date-input">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="Telf" class="col-sm-10">Sueldo:</label>
                                        <input type="number" placeholder="Sueldo" class="form-control" name="sueldo">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <h5><label>Genero:</label></h5>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="radio1" name="genero" value="1">
                                            <label class="radio-inline" for="radio1">Masculino</label>
                                            <input type="radio" class="form-check-input" id="radio2" name="genero" value="0">
                                            <label class="radio-inline" for="radio2">Femenino</label>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" id="btnguardar" type="submit">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbEmpleados').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#tbEmpleados tbody').on('click', '.editar', function () {

                var fila = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/empleado/" + fila,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        $("input[name='id']").val(rta.id);
                        $("input[name='nombre']").val(rta.nombre);
                        $("input[name='apellidos']").val(rta.apellidos);
                        $("input[name='documento']").val(rta.documento);
                        $("input[name='edad']").val(rta.edad);
                        $("select[name='tipo_contratacion']").val(rta.tipo_contratacion);
                        $("input[name='fecha_ingreso']").val(rta.fecha_ingreso);
                        $("input[name='telefono']").val(rta.telefono);
                        $("input[name='sueldo']").val(rta.sueldo);
                        $("input[name='genero'][value='" + rta.genero + "']").prop('checked', true);

                        $("#frmajax").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#frmajax").attr('action', '{{ url('/empleado') }}/' + rta.id);
                    }
                });
            });

            $('#tbEmpleados tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var nombre = $(this).parent().parent().children("td").eq(1).html();
                var apellidos = $(this).parent().parent().children("td").eq(2).html();
                var documento = $(this).parent().parent().children("td").eq(3).html();

                var aux = documento + ": " + nombre + " " + apellidos;

                alertify.confirm("¿Desea eliminar a \n" + aux + "?",

                    function () {
                        $.ajax({
                            url: "/empleado/" + id,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rta) {
                                if (rta.OK == 1) {
                                    fila.remove();
                                    alertify.success('Borrado ' + documento + ": " + nombre + " " + apellidos);
                                }
                            }
                        });
                    }
                );
            });

            $("select[name='tipo_contratacion']").change(function () {
                var inputSueldo = $("input[name='sueldo']");
                if($("select[name='tipo_contratacion']").val() == "TC"){
                    inputSueldo.prop('readonly', false);
                }else{
                    inputSueldo.val("");
                    inputSueldo.prop('readonly', true);
                }
            });
        });
    </script>
@endsection

