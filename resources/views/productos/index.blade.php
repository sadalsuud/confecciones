@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>

@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Registro de productos
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span> Registrar
                                              </button>
                                            </div>
                                    </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbProductos" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Referencia</th>
                                    <th>Descripcion</th>
                                    <th>Talla</th>
                                    <th>Precio venta</th>
                                    <th>Costo</th>
                                    {{--<th>Cantidad</th>--}}
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($productos as $producto)
                                    <tr>
                                        <td>{{ $producto->id}}</td>
                                        <td>{{ $producto->nombre }}</td>
                                        <td>{{ $producto->referencia }}</td>
                                        <td>{{ $producto->descripcion }}</td>
                                        <td>{{ $producto->talla }}</td>
                                        <td>{{ $producto->precio_venta }}</td>
                                        <td>{{ $producto->costo }}</td>
{{--                                        <td>{{ $producto->cantidad }}</td>--}}
                                        <td><a class='btn btn-danger eliminar' data-mm='modal'><span class='fas fa-trash-alt'></span></a>
                                            <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo">
                                                <span class='fas fa-edit'></span>
                                            </a>
                                            <a class='btn btn-info addCantidad' data-toggle="modal" href="#modalAddCantidad">
                                                <span class='fas fa-plus-square'></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Referencia</th>
                                    <th>Descripcion</th>
                                    <th>Talla</th>
                                    <th>Precio venta</th>
                                    <th>Costo</th>
                                    {{--<th>Cantidad</th>--}}
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 1-->
            <form action="{{ url('/producto') }}" method="POST" id="frmajax">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Nuevo producto</h4>
                            </div>
                            <div class="modal-body">

                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="concepto">Nombre:</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="precio_venta">Precio de venta:</label>
                                        <input type="number" min="0" class="form-control" id="precio_venta" placeholder="Precio de venta"
                                               name="precio_venta">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-4">
                                        <label for="precio_venta">Costo:</label>
                                        <input type="number" min="0" class="form-control" id="costo" placeholder="Costo"
                                               name="costo">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="descripcion">Descripcion:</label>
                                        <textarea name="descripcion" rows="5" cols="30"></textarea>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label for="cantidad">Referencia:</label>
                                        <input type="text" class="form-control" id="referencia" placeholder="Código de referencia" name="referencia">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 ">
                                        <label for="tipDoc">Talla</label>
                                        <select name="talla" class="form-control" id="talla">
                                            <option value="">Seleccione...</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="XL">XL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" type="submit">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            <!-- Modal 2-->
            <form action="{{ url('/agregarEjemplar') }}" method="POST" id="formAddEjemplar">
                @csrf
                <input type="hidden" name="producto_id">
                <div class="modal fade" id="modalAddCantidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Nuevo ejemplar</h4>
                                <h3 id="detalleProducto"></h3>
                            </div>
                            <div class="modal-body">

                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-5">
                                        <label for="tipDoc">Pedido:</label>
                                        <select name="pedido_id" class="form-control" id="pedido_id" required>
                                            <option value="">Seleccione...</option>
                                            @foreach($pedidos as $index => $pedido)
                                                <option value="{{ $index }}">{{ $pedido }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-5">
                                        <label for="tipDoc">Empleado:</label>
                                        <select name="empleado_id" class="form-control" id="empleado_id" required>
                                            <option value="">Seleccione...</option>
                                            @foreach($empleados as $index => $empleado)
                                                <option value="{{ $index }}">{{ $empleado }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-2">
                                        <label for="precio_venta">Cantidad:</label>
                                        <input type="number" min="0" value="0" class="form-control" id="cantidad" name="cantidad" required>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-default pull-right" id="btnAddEjemplar">
                                        <i class="fas fa-save"></i> Guardar Registro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>


    <script>
        $(document).ready(function () {

            $('#tbProductos').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $('#tbProductos tbody').on('click', '.editar', function () {

                var fila = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/producto/" + fila,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        $("input[name='id']").val(rta.id);
                        $("input[name='nombre']").val(rta.nombre);
                        $("input[name='referencia']").val(rta.referencia);
                        $("textarea[name='descripcion']").val(rta.descripcion);
                        $("input[name='precio_venta']").val(rta.precio_venta);
                        $("input[name='costo']").val(rta.costo);
                        $("select[name='talla']").val(rta.talla);

                        $("#frmajax").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#frmajax").attr('action', '{{ url('/producto') }}/' + rta.id);
                    }
                });
            });

            $('#tbProductos tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var referencia = $(this).parent().parent().children("td").eq(2).html();

                alertify.confirm("¿Desea eliminar el producto " + referencia + "?",

                    function () {
                        $.ajax({
                            url: "/producto/" + id,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rta) {
                                if (rta.OK == 1) {
                                    fila.remove();
                                    alertify.success('Borrado ' + referencia);
                                }
                            }
                        });
                    }
                );
            });

            $('#tbProductos tbody').on('click', '.addCantidad', function () {

                var id = $(this).parent().parent().children("td").eq(0).html();
                var nombre = $(this).parent().parent().children("td").eq(1).html();
                var referencia = $(this).parent().parent().children("td").eq(2).html();
                var talla = $(this).parent().parent().children("td").eq(4).html();
                var precio = $(this).parent().parent().children("td").eq(5).html();

                var aux = referencia + " | " + nombre + " | " + "Talla: " + talla + " | " + "Precio: $" + precio;
                $("#detalleProducto").text(aux);
                $("input[name='producto_id']").val(id);
                $("#cantidad").val(0);
            });

            $('#btnAddEjemplar').click(function (event) {

                event.preventDefault();

                var producto = $("input[name='producto_id']").val();
                var pedido = $("select[name='pedido_id']").val();
                var cantidad = $("#cantidad").val();

                if (pedido != "") {

                    $.ajax({
                        url: "/validadCantidad",
                        type: 'get',
                        dataType: 'json',
                        data: {"producto_id": producto, "pedido_id": pedido, "cantidad": cantidad},
                        success: function (rta) {

                            if (rta == "false") {
                                alertify.error('Excede la cantidad original de productos del pedido');
                            } else {
                                $('#formAddEjemplar').submit();
                                console.log("envió el formulario");
                            }
                        }
                    });

                }


            });
        });
    </script>
@endsection

