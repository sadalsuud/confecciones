@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>

@endsection

@section('content')

    <section class="content-header">
        <div id="template_alerts"></div>
        <h1>
            Información pedidos
            <small></small>
            <span class="text-muted pull-right" style="font-size: 10px;">Martes 24 de Abril de 2018</span>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="panel-heading">
                                <h4><i class="fa fa-search"></i>
                                    Consulta General
                                    <span class="pull-right">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mimodalejemplo">
                                                <span class="fa fa-users"></span> Registrar
                                              </button>
                                            </div>
                                    </span>
                                </h4>
                            </div>
                        </div>
                        <div class="box-body">

                            <table id="tbPedidos" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Fecha de entrega</th>
                                    <th>Cliente</th>
                                    <th>Total</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pedidos as $pedido)
                                    @if($pedido->estado == 0)
                                        <tr class="warning">
                                    @elseif($pedido->estado == 1)
                                        <tr class="danger">
                                    @elseif($pedido->estado == 2)
                                        <tr>
                                    @elseif($pedido->estado == 3)
                                        <tr class="success">
                                            @endif
                                            <td>{{ $pedido->id}}</td>
                                            <td>{{ $pedido->fecha_entrega }}</td>
                                            <td>{{ $pedido->cliente->nombre }}</td>
                                            <td>{{ $pedido->total }}</td>
                                            @if($pedido->estado == 0)
                                                <td>Cotización</td>
                                            @elseif($pedido->estado == 1)
                                                <td>Pedido no pago</td>
                                            @elseif($pedido->estado == 2)
                                                <td>Pedido terminado</td>
                                            @elseif($pedido->estado == 3)
                                                <td>Pedido pago</td>
                                            @endif
                                            <td>{{ $pedido->descripcion }}</td>
                                            <td><a class='btn btn-danger eliminar'><span class='fas fa-trash-alt'></span></a>
                                                <a class='btn btn-success editar' data-toggle="modal" href="#mimodalejemplo">
                                                    <span class='fas fa-edit'></span>
                                                </a>
                                                @if($pedido->estado != 3)
                                                    <a class='btn btn-primary cobrar'><span class='fas fa-donate'></span></a>
                                                @endif
                                                <a class='btn btn-info pdf' href="{{ route('pedido.pdf', $pedido->id) }}">
                                                    <span class='fas fa-file-pdf'></span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Fecha de entrega</th>
                                    <th>Cliente</th>
                                    <th>Total</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <form action="{{ url('/pedido') }}" method="POST" id="formAddPedido">
                @csrf
                <div class="modal fade" id="mimodalejemplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Registro Pedido</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">

                                    <div class="form-group col-xs-12 col-sm-3">
                                        <label for="tipDoc">Cliente:</label>
                                        <select class="form-control" name="cliente_id" id="cliente_id">
                                            <option value="">Seleccione</option>
                                            @foreach($clientes as $index => $cliente)
                                                <option value="{{ $index }}">{{ $cliente }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-2">
                                        <label for="date" class="col-2 col-form-label">Fecha Entrega</label>
                                        <input class="form-control" type="date" name="fecha_entrega" id="example-date-input">
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-2">
                                        <label for="total">Valor Total:</label>
                                        <input type="text" class="form-control" value="0" placeholder="Valor Pedido" name="total" readonly>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-2">
                                        <label for="total">Tipo:</label>
                                        <select class="form-control" name="estado">
                                            <option value="">Seleccione</option>
                                            <option value="0">Cotización</option>
                                            <option value="1">Pedido no pago</option>
                                            <option value="2">Pedido terminado</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-2">
                                        <label for="total">Agregar:</label>
                                        <div>
                                            <a class='btn btn-success agregarRenglon'><span class='fas fa-plus-square'></span></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row detalle">

                                    <div class="renglon">
                                        <div class="col-xs-12 col-sm-3">
                                            <input class="form-control cantidad" name="cantidad[]" min="0" type="number" placeholder="Cantidad">
                                        </div>

                                        <div class="col-xs-12 col-sm-5">
                                            <select class="form-control producto" name="productos[]">
                                                <option value="">Seleccione</option>
                                                @foreach($productos as $producto)
                                                    <option value="{{ $producto->id }}">{{ $producto->nombre ." | Ref: ". $producto->referencia ." | Talla: ". $producto->talla }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-xs-12 col-sm-4">
                                            <div class="">
                                                <a class='btn btn-danger eliminarRenglon'><span class='fas fa-minus-square'></span></a>
                                                <input type="hidden" class="subtotal" value="0">
                                                <input type="hidden" class="precio_venta" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-12">
                                        <label for="descripcion">Descripcion:</label>
                                        <textarea class="form-control" rows="3" name="descripcion" id="descripcion"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                                <button class="btn btn-default pull-right" type="submit">
                                    <i class="fas fa-save"></i> Guardar Registro
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#tbPedidos').DataTable({
                language: {
                    "url": "/js/Spanish.json"
                }
            });

            $(".detalle").on('change', '.producto', function () {

                var producto = $(this);
                console.log(producto.html());

                $.ajax({
                    url: "/getProducto",
                    type: 'POST',
                    dataType: 'json',
                    data: {"producto_id": producto.val()},
                    success: function (rta) {

                        var cantidad = producto.parent().parent().find("input[type='number']").val();
                        var sub = parseInt(cantidad) * parseInt(rta.precio_venta);
                        var subtotal = producto.parent().parent().find("input[type='hidden']");
                        subtotal.val(sub);
                        var total = 0;

                        $(".subtotal").each(function () {
                            total += parseInt($(this).val());
                        });

                        $("input[name='total']").val(total);
                    }
                });
            });

            $('.detalle').on('change', '.cantidad', function () {

                var cantidad = $(this).val();
                var producto = $(this).parent().parent().find("select");

                $.ajax({
                    url: "/getProducto",
                    type: 'POST',
                    dataType: 'json',
                    data: {"producto_id": producto.val()},
                    success: function (rta) {

                        // si el json no está vacio
                        if (Object.keys(rta).length !== 0) {

                            var sub = parseInt(cantidad) * parseInt(rta.precio_venta);
                            var subtotal = producto.parent().parent().find("input[type='hidden']");
                            subtotal.val(sub);
                            var total = 0;

                            $(".subtotal").each(function () {
                                total += parseInt($(this).val());
                            });

                            $("input[name='total']").val(total);
                        }
                    }
                });
            });

            $(".agregarRenglon").click(function () {

                var anterior = $(".renglon:last");
                var producto_id = anterior.find(".producto").val();
                var opciones = anterior.find(".producto option").length;

                if (producto_id != "" && opciones > 2) {
                    // clonar el renglon y quitar una opción o sea un producto
                    var nuevo = anterior.clone(true, true);
                    nuevo.find(".producto option[value='" + producto_id + "']").remove();
                    $(".detalle").append(nuevo);
                }
            });

            $(".detalle").on("click", ".eliminarRenglon", function () {
                if ($(".renglon").length > 1) {
                    $(this).parent().parent().parent().remove();
                    var total = 0;
                    // actualizar el valor del total
                    $(".subtotal").each(function () {
                        total += parseInt($(this).val());
                    });

                    $("input[name='total']").val(total);
                }
            });

            $('#valor_unitario').blur(function () {
                var total = parseInt($("#cantidad").val()) * parseInt($("#valor_unitario").val());
                $('#total').val(total);
            });

            $('#tbPedidos tbody').on('click', '.eliminar', function () {

                var fila = $(this).parent().parent();
                var id = $(this).parent().parent().children("td").html();
                var cliente = $(this).parent().parent().children("td").eq(2).html();
                var vtotal = $(this).parent().parent().children("td").eq(3).html();

                var aux = cliente + ": " + vtotal;

                alertify.confirm("¿Desea eliminar a \n" + aux + "?",

                    function () {
                        $.ajax({
                            url: "/pedido/" + id,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rta) {
                                if (rta.OK == 1) {
                                    fila.remove();
                                    alertify.success('Borrado el pedido a ' + aux);
                                }
                            }
                        });
                    }
                );
            });

            $('#tbPedidos tbody').on('click', '.editar', function () {

                var id = $(this).parent().parent().children("td").html();

                $.ajax({
                    url: "/pedido/" + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (rta) {

                        rta = rta[0];
                        limpiarFormulario();

                        $("input[name='id']").val(rta.id);
                        $("#cliente_id").val(rta.cliente_id);
                        $("input[name='fecha_entrega']").val(rta.fecha_entrega);
                        if (rta.estado == 3) {
                            $("select[name='estado']").val("");
                        } else {
                            $("select[name='estado']").val(rta.estado);
                        }
                        $("input[name='total']").val(rta.total);
                        $("textarea[name='descripcion']").val(rta.descripcion);

                        // a mostrar los productos
                        var renglon = "";
                        rta.productos.forEach(function (producto, index) {
                            if (index == 0) {
                                renglon = $(".renglon:last");
                                renglon.find(".cantidad").val(producto.pivot.cantidad);
                                renglon.find(".producto").val(producto.id);
                                renglon.find("input[type='hidden']").val(parseInt(producto.pivot.cantidad) * parseInt(producto.precio_venta));
                            }
                            else {
                                renglon = $(".renglon:last").clone(true, true);
                                renglon.find(".cantidad").val(producto.pivot.cantidad);
                                renglon.find(".producto").val(producto.id);
                                renglon.find("input[type='hidden']").val(parseInt(producto.pivot.cantidad) * parseInt(producto.precio_venta));
                                $(".detalle").append(renglon);
                            }
                        });

                        $("#formAddPedido").append("<input type='hidden' name='_method' value='PUT'>");
                        $("#formAddPedido").attr('action', '{{ url('/pedido') }}/' + rta.id);
                    }
                });
            });

            $("#mimodalejemplo").on('show.bs.modal', function () {
                limpiarFormulario();
            });

            function limpiarFormulario() {
                var formulario = $("#formAddPedido");
                formulario.find("input[name='_method']").remove();
                formulario.attr('action', '{{ url('/pedido') }}');
                $("#formAddPedido")[0].reset();
                var detalle = $(".detalle");
                var primerRenglon = detalle.find(".renglon:first");
                detalle.empty();
                detalle.append(primerRenglon);
            }

            $('#tbPedidos tbody').on('click', '.cobrar', function () {

                var botonCobro = $(this);
                var id = $(this).parent().parent().children("td").html();
                var cliente = $(this).parent().parent().children("td").eq(2).html();
                var vtotal = $(this).parent().parent().children("td").eq(3).html();

                var aux = vtotal + " de " + cliente;

                alertify.confirm("¿Desea cobrar $" + aux + "?",

                    function () {
                        $.ajax({
                            url: "/cobrarPedido",
                            type: 'post',
                            dataType: 'json',
                            data: {"pedido_id": id},
                            success: function (rta) {
                                if (rta) {
                                    botonCobro.parent().parent().children("td").eq(4).text("Pedido pago");
                                    botonCobro.parent().parent().removeClass("danger");
                                    botonCobro.parent().parent().addClass("success");
                                    botonCobro.remove();
                                    alertify.success('Pedido cobrado correctamente');
                                }
                            }
                        });
                    }
                );
            });


        });
    </script>
@endsection


