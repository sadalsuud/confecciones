@extends('layouts.app')


@section('content')


    <div class="row">

        @php
         $pedido = $pedido->first();
        @endphp
        <div class="form-group col-xs-12 col-sm-3">
            <label for="tipDoc">Cliente: {{ $pedido->cliente->nombre }}</label>
        </div>

        <div class="form-group col-xs-12 col-sm-2">
            <label for="date" class="col-2 col-form-label">Fecha Entrega: {{ $pedido->fecha_entrega }}</label>
        </div>

        <div class="form-group col-xs-12 col-sm-2">
            <label for="total">Valor Total: {{ $pedido->total }}</label>
        </div>

    </div>

    <div class="row detalle">

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Cantidad</th>
                <th>Producto</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pedido->productos as $producto)
                <tr>
                    <td>{{ $producto->pivot->cantidad }}</td>
                    <td>{{ $producto->id }}">{{ $producto->nombre ." | Ref: ". $producto->referencia ." | Talla: ". $producto->talla }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="form-group col-xs-12 col-sm-12">
            <label for="descripcion">Descripcion:</label>
            <textarea class="form-control" rows="3" name="descripcion" id="descripcion">{{ $pedido->descripcion }}</textarea>
        </div>
    </div>

@endsection