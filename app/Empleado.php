<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleado extends Model
{
    use SoftDeletes;

    protected $table = 'empleado';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre', 'apellidos', 'documento', 'fecha_ingreso', 'tipo_contratacion', 'genero', 'edad', 'telefono', 'sueldo'];

    public function epp()
    {
        return $this->hasMany(Epp::class);
    }
}
