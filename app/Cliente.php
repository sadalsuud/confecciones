<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';

    protected $fillable = ['nombre', 'documento', 'telefono', 'direccion'];

    public function pedidos(){
        return $this->hasMany(Pedido::class);
    }
}
