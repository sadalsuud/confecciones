<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contabilidad extends Model
{
    protected $table = 'contabilidad';

    protected $fillable = ['tipo', 'concepto', 'cantidad', 'unidad', 'valor_unidad', 'total'];

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function pedido(){
        return $this->belongsTo(Pedido::class);
    }
}
