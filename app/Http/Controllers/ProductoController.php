<?php

namespace App\Http\Controllers;

use App\Epp;
use App\Pedido;
use App\Empleado;
use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::all();
        $empleados = Empleado::all()->pluck('nombre', 'id');
        $pedidos = [];
        Pedido::all()->each(function ($p) use (&$pedidos) {
            $pedidos["$p->id"] = "Id: " . $p->id . " | " . $p->fecha_entrega . " | " . $p->cliente->nombre;
        });

        return view('productos.index', compact(['productos', 'empleados', 'pedidos']));
    }

    public function getProducto(Request $request)
    {
        $producto = Producto::find($request->input('producto_id'));
        return response()->json($producto);
    }

    public function agregarEjemplar(Request $request)
    {
        // agrega en el stock del producto
        $producto = Producto::find($request->input('producto_id'));
        $producto->cantidad = $producto->cantidad + $request->input('cantidad');
        $producto->save();

        // guardar la cantidad de producto hecha por un empleado para ése pedido
        $epp = Epp::where('empleado_id', $request->input('empleado_id'))
            ->where('producto_id', $request->input('producto_id'))
            ->where('pedido_id', $request->input('pedido_id'))
            ->first();

        if (is_null($epp)) {
            $epp = new Epp();
            $epp->empleado_id = $request->input('empleado_id');
            $epp->producto_id = $request->input('producto_id');
            $epp->pedido_id = $request->input('pedido_id');
            $cantidad = 0;
        } else {
            $cantidad = $epp->cantidad;
        }

        $epp->cantidad = $cantidad + $request->input('cantidad');
        $epp->save();

        return redirect()->route('producto.index');
    }

    public function validarCantidad(Request $request)
    {

        // busca el registro en la tabla pivote
        $objetivo = DB::table('pedido_producto')->where('pedido_id', $request->input('pedido_id'))
            ->where('producto_id', $request->input('producto_id'))
            ->select('cantidad')
            ->first();

        $loProducido = Epp::where('producto_id', $request->input('producto_id'))
            ->where('pedido_id', $request->input('pedido_id'))
            ->sum('cantidad');

//        dd($objetivo);

        // si es el primer registro de ejemplares o ap
//        if ($request->input('cantidad') <= $objetivo->cantidad) {
//            return response()->json("true");
//        } else

        if (($request->input('cantidad') + $loProducido) <= $objetivo->cantidad) {
//        dd("->>" . ($request->input('cantidad') + $loProducido));
            return response()->json("true");
        }

        return response()->json("false");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Producto::create($request->all());
        return redirect()->route('producto.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::find($id);
        return response()->json($producto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->fill($request->all());
        $producto->save();

        return redirect()->route('producto.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aux = Producto::destroy($id);
        return response()->json(["OK" => $aux]);
    }
}
