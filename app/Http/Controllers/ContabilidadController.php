<?php

namespace App\Http\Controllers;

use App\Contabilidad;
use App\Empleado;
use Carbon\Carbon;
use App\Proveedor;
use Illuminate\Http\Request;

class ContabilidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::orderBy('nombre')->pluck('nombre', 'id');
        $cuentas = Contabilidad::all();
        return view('contabilidad.index', compact(['cuentas', 'proveedores']));
    }

    public function liquidarNomina(Request $request)
    {
        if ($request->has('desde') && $request->has('hasta')) {

            $desde = Carbon::createFromFormat("Y-m-d", $request->input('desde'));
            $hasta = Carbon::createFromFormat("Y-m-d", $request->input('hasta'));
            $data = Empleado::with('productos')->get();
            $diff = $desde->diffInDays($hasta);

            $empleados = [];
            $data->each(function ($empleado) use (&$empleados, $desde, $hasta, $diff) {

                $aux = [];
                $aux["nombre"] = $empleado->nombre . " " . $empleado->apellidos;
                $aux["documento"] = $empleado->documento;
                $aux["produccion"] = $empleado->productos()->whereBetween('terminado', [$desde, $hasta])->count();
                if (strcmp($empleado->tipo_contratacion, "TC") == 0) {
                    $aux["tipo_contratacion"] = "Tiempo completo";
                    $aux["liquidacion"] = ($empleado->sueldo / 30) * $diff;
                } else {
                    $aux["tipo_contratacion"] = "Ocasional";
                    $productos = $empleado->productos()->whereBetween('terminado', [$desde, $hasta])->get();

                    $sumatoria = 0;
                    $productos->each(function ($producto) use (&$sumatoria) {
                        $sumatoria += $producto->costo * $producto->pivot->cantidad;
                    });

                    $aux["liquidacion"] = $sumatoria;
                }
                $empleados[] = $aux;
            });
            return view('contabilidad.liquidar-nomina', compact('empleados'));
        }
        return view('contabilidad.liquidar-nomina');
    }

    public function guardarLiquidarNomina(Request $request)
    {

        $nuevo = new Contabilidad();
        $nuevo->tipo = 0;   // sale dinero
        $nuevo->concepto = "Pago de nomina el " . date("d-m-Y");
        $nuevo->total = $request->input('nomina');

        $anterior = Contabilidad::latest()->first();
        if (is_null($anterior)) {
            $saldo = 0;
        } else {
            $saldo = $anterior->saldo;
        }

        $nuevo->saldo = $saldo - $nuevo->total;
        $nuevo->save();

        return response()->json(["OK" => 1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registro = new Contabilidad();
        $registro->fill($request->all());
        if ($request->filled('proveedor_id')) {
            $registro->proveedor_id = $request->input('proveedor_id');
        }

        $anterior = Contabilidad::latest()->first();
        if (is_null($anterior)) {
            $saldo = 0;
        } else {
            $saldo = $anterior->saldo;
        }
        // entra dinero
        if ($request->input('tipo') == 1) {
            $registro->saldo = $saldo + $request->input('total');
        } else {
            $registro->saldo = $saldo - $request->input('total');
        }
        $registro->save();
        return redirect()->route('contabilidad.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registro = Contabilidad::find($id);
        return response()->json($registro);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $registro = Contabilidad::find($id);
        $registro->fill($request->all());
        $registro->save();

        return redirect()->route('contabilidad.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aux = Contabilidad::destroy($id);
        return response()->json(["OK" => $aux]);
    }
}
