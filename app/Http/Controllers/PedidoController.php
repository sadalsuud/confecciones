<?php

namespace App\Http\Controllers;

use App\Pedido;
use App\Producto;
use App\Cliente;
use App\Contabilidad;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::all();
        $productos = Producto::all();
        $clientes = Cliente::all()->pluck('nombre', 'id');
        return view('pedidos.index', compact(['pedidos', 'productos', 'clientes']));
    }

    public function cobrarPedido(Request $request)
    {
        $pedido = Pedido::find($request->input('pedido_id'));

        if ($pedido->estado != 3) {
            $pedido->estado = 3;
            $pedido->save();

            // registrar el pago en el libro de contabilidad
            $anterior = Contabilidad::latest()->first();
            // si hay registro anterior
            $nuevoRegistro = new Contabilidad();
            $nuevoRegistro->tipo = 1;      // pago
            $nuevoRegistro->concepto = "Pago de pedido # " . $pedido->id . " con " . $pedido->productos()->count() . " prendas";
            $nuevoRegistro->total = $pedido->total;
            $nuevoRegistro->pedido_id = $pedido->id;
            if ($anterior) {
                $nuevoRegistro->saldo = $anterior->saldo + $pedido->total;
            } else {
                $nuevoRegistro->saldo = $pedido->total;
            }
            $rta = $nuevoRegistro->save();

            return response()->json($rta);
        }
        return response()->json(false);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pedido = Pedido::create($request->all());
        $productos = $request->input('productos');
        $cantidad = $request->input('cantidad');

        $aux = [];
        for ($i = 0; $i < count($productos); $i++) {
            $aux[$productos[$i]] = ['cantidad' => $cantidad[$i]];
        }
        $pedido->productos()->attach($aux);

        return redirect()->route('pedido.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::where('id', $id)->with('productos')->get();
        return response()->json($pedido);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pedido = Pedido::find($id);
        $pedido->fill($request->all());
        $pedido->save();
        $productos = $request->input('productos');
        $cantidad = $request->input('cantidad');

        $aux = [];
        for ($i = 0; $i < count($productos); $i++) {
            $aux[$productos[$i]] = ['cantidad' => $cantidad[$i]];
        }
        $pedido->productos()->sync($aux);

        return redirect()->route('pedido.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aux = Pedido::destroy($id);
        return response()->json(["OK" => $aux]);
    }

    public function pdf($id){

        $pedido = Pedido::where('id', $id)->with('productos')->first();
        $pdf = PDF::loadView('pedidos.pdf', compact('pedido'));

        return $pdf->download('listado.pdf');

    }
}
