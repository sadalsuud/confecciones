<?php

namespace App\Http\Controllers;

use App\Empleado;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleado::all();
        return view('empleados.index', compact('empleados'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Empleado::create($request->all());
        return redirect()->route('empleado.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado = Empleado::find($id);
        return response()->json($empleado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empleado = Empleado::find($id);
        $empleado->fill($request->all());
        $empleado->save();

        return redirect()->route('empleado.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aux = Empleado::destroy($id);
        return response()->json(["OK" => $aux]);
    }

    public function historico()
    {
        $eliminados = Empleado::onlyTrashed()->get();
        return view('empleados.historico', compact('eliminados'));
    }

    public function eliminar(Request $request)
    {
        $eliminado = Empleado::withTrashed()->where('id' , $request->input('id'))->first();
        $eliminado->forceDelete();
        return response()->json(["ok" => "Eliminado permanentemente"]);
    }

    public function restaurar(Request $request)
    {
        $exEliminado = Empleado::withTrashed()->where('id' , $request->input('id'))->first();
        $exEliminado->restore();
        return response()->json(["ok" => $exEliminado->documento ." restaurado"]);
    }
}
