<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Producto extends Model
{
    protected $table = 'producto';
    protected $dates = ['deleted_at'];


    protected $fillable = ['nombre', 'referencia', 'descripcion', 'talla', 'precio_venta', 'costo'];

    public function pedidos()
    {
        return $this->belongsToMany(Pedido::class)->withPivot('cantidad');
    }

//    public function empleados(){
//        return $this->belongsToMany(Empleado::class)->withPivot('cantidad', 'terminado');
//    }

    public function epp()
    {
        return $this->hasMany(Epp::class);
    }
}
