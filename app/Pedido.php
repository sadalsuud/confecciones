<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedido';

    protected $fillable = ['fecha_entrega', 'descripcion', 'total', 'estado', 'cliente_id'];

    public function productos(){
        return $this->belongsToMany(Producto::class)->withPivot('cantidad');
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function epp()
    {
        return $this->hasMany(Epp::class);
    }
}
