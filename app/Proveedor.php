<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedor';

    protected $fillable = ['nombre', 'nit', 'telefono', 'direccion'];

    public function compras(){
        return $this->hasMany(Contabilidad::class);
    }
}
