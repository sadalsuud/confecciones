<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epp extends Model
{
    protected $table = 'empleado_pedido_producto';

    protected $dates = ['deleted_at'];

    protected $fillable = ['cantidad'];

    public function productos()
    {
        return $this->belongsTo(Producto::class);
    }

    public function empleados()
    {
        return $this->belongsTo(Empleado::class);
    }

    public function pedidos()
    {
        return $this->belongsTo(Pedido::class);
    }
}
